<?php

namespace AzureSpring\Piaofutong\Notification;

use JMS\Serializer\Annotation as Serializer;

trait Action
{
    /**
     * @var \DateTimeImmutable
     *
     * @Serializer\SerializedName("ActionTime")
     * @Serializer\Type("DateTimeImmutable<'Y-m-d H:i:s', 'Asia/Shanghai'>")
     */
    private $createdAt;

    /**
     * @var int|null
     *
     * @Serializer\SerializedName("Action")
     * @Serializer\Type("int")
     */
    private $kind;

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getKind(): ?int
    {
        return $this->kind;
    }
}
