<?php

namespace AzureSpring\Piaofutong\Notification;

use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\Discriminator(field="OrderState", map={
 *     "1" = VerificationNotification::class,
 *     "2" = VerificationCodeNotification::class,
 *     "6" = TicketNotification::class,
 *     "7" = VerificationNotification::class,
 *     "8" = RefundNotification::class,
 * })
 */
abstract class AbstractNotification
{
    const TYPE_VERIFICATION_FULL = 1;
    const TYPE_CODE = 2;
    const TYPE_ON_SHELVES = 2;
    const TYPE_OFF_SHELVES = 3;
    const TYPE_PRICE = 4;
    const TYPE_PRODUCT = 5;
    const TYPE_TICKET = 6;
    const TYPE_VERIFICATION_PARTIAL = 7;
    const TYPE_REFUND = 8;

    /**
     * @var string
     *
     * @Serializer\SerializedName("VerifyCode")
     * @Serializer\Type("string")
     */
    private $signature;

    /**
     * @var int
     *
     * @Serializer\SerializedName("OrderState")
     * @Serializer\Type("string")
     */
    private $type;

    public function getSignature(): string
    {
        return $this->signature;
    }

    public function getType(): int
    {
        return $this->type;
    }
}
