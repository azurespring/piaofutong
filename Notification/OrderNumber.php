<?php

namespace AzureSpring\Piaofutong\Notification;

use JMS\Serializer\Annotation as Serializer;

trait OrderNumber
{
    /**
     * @var string
     *
     * @Serializer\SerializedName("Order16U")
     * @Serializer\Type("string")
     */
    private $id;

    /**
     * @var string
     *
     * @Serializer\SerializedName("OrderCall")
     * @Serializer\Type("string")
     */
    private $permanentId;

    public function getId(): string
    {
        return $this->id;
    }

    public function getPermanentId(): string
    {
        return $this->permanentId;
    }
}
