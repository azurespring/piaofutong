<?php

namespace AzureSpring\Piaofutong\Notification;

use JMS\Serializer\Annotation as Serializer;

class RefundNotification extends VerificationNotification
{
    const RESULT_ACCEPTED = 1;
    const RESULT_REJECTED = 2;

    /**
     * @var int
     *
     * @Serializer\SerializedName("Refundtype")
     * @Serializer\Type("int")
     */
    private $result;

    /**
     * @var string
     *
     * @Serializer\SerializedName("RemoteSn")
     * @Serializer\Type("string")
     */
    private $sn;

    /**
     * @var string
     *
     * @Serializer\SerializedName("RefundFee")
     * @Serializer\Type("int")
     */
    private $fee;

    /**
     * @var string
     *
     * @Serializer\SerializedName("RefundAmount")
     * @Serializer\Type("int")
     */
    private $subtotal;

    /**
     * @var string
     *
     * @Serializer\SerializedName("Explain")
     * @Serializer\Type("string")
     */
    private $remarks;

    public function getResult(): int
    {
        return $this->result;
    }

    public function getSn(): string
    {
        return $this->sn;
    }

    public function getFee(): string
    {
        return $this->fee;
    }

    public function getSubtotal(): string
    {
        return $this->subtotal;
    }

    public function getRemarks(): string
    {
        return $this->remarks;
    }
}
