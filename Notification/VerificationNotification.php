<?php

namespace AzureSpring\Piaofutong\Notification;

use JMS\Serializer\Annotation as Serializer;

class VerificationNotification extends AbstractNotification
{
    use Action;
    use OrderNumber;

    /**
     * @var int
     *
     * @Serializer\SerializedName("Tnumber")
     * @Serializer\Type("int")
     */
    private $quantity;

    /**
     * @var int
     *
     * @Serializer\SerializedName("AllCheckNum")
     * @Serializer\Type("int")
     */
    private $verifiedQuantity;

    /**
     * @var int
     *
     * @Serializer\SerializedName("Source")
     * @Serializer\Type("int")
     */
    private $source;

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getVerifiedQuantity(): int
    {
        return $this->verifiedQuantity;
    }

    public function getSource(): int
    {
        return $this->source;
    }
}
