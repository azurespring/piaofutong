<?php

namespace AzureSpring\Piaofutong\Notification;

use JMS\Serializer\Annotation as Serializer;

class VerificationCodeNotification extends AbstractNotification
{
    /**
     * @var string
     *
     * @Serializer\SerializedName("pftOrder")
     * @Serializer\Type("string")
     */
    private $id;

    /**
     * @var string
     *
     * @Serializer\SerializedName("remoteOrder")
     * @Serializer\Type("string")
     */
    private $permanentId;

    /**
     * @var string
     *
     * @Serializer\SerializedName("code")
     * @Serializer\Type("string")
     */
    private $code;

    public function getId(): string
    {
        return $this->id;
    }

    public function getPermanentId(): string
    {
        return $this->permanentId;
    }

    public function getCode(): string
    {
        return $this->code;
    }
}
