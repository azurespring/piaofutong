<?php

namespace AzureSpring\Piaofutong;

use AzureSpring\Piaofutong\Annotation\Template;
use AzureSpring\Piaofutong\Model;
use AzureSpring\Piaofutong\Model\Money;
use AzureSpring\Piaofutong\Model\Result;
use AzureSpring\Piaofutong\Model\Status;
use AzureSpring\Piaofutong\Notification\AbstractNotification;
use AzureSpring\Piaofutong\Serializer\TemplateHandler;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\Handler\HandlerRegistryInterface;
use JMS\Serializer\SerializerBuilder;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\StreamFactoryInterface;

class Client
{
    private $responseFactory;

    private $streamFactory;

    private $soap;

    private $username;

    private $password;

    private $serializer;

    private $contents;

    private $tz;

    public function __construct(ResponseFactoryInterface $responseFactory, StreamFactoryInterface $streamFactory, string $wsdl, string $username, string $password, array $soapClient = [])
    {
        $this->responseFactory = $responseFactory;
        $this->streamFactory = $streamFactory;
        $this->soap = new \SoapClient($wsdl, $soapClient);
        $this->username = $username;
        $this->password = $password;
        $this->serializer = SerializerBuilder::create()
            ->setMetadataDirs([
                'AzureSpring\\Piaofutong\\Model' => __DIR__.'/Resources/config/serializer',
            ])
            ->configureHandlers(function (HandlerRegistryInterface $registry) {
                $registry->registerSubscribingHandler(new TemplateHandler());
            })
            ->addDefaultHandlers()
            ->build()
        ;

        $this->tz = new \DateTimeZone('Asia/Shanghai');
    }

    /** @return Model\Product[] */
    public function findProducts(int $offset, int $limit)
    {
        return $this->request(Model\Product::class, 'Get_ScenicSpot_List', [$offset, $offset + $limit]);
    }

    /** @return Model\Product */
    public function findProduct(string $id)
    {
        return $this->fetch(Model\Product::class, 'Get_ScenicSpot_Info', [$id]);
    }

    /** @return Model\Ticket[] */
    public function findTickets(?string $productId, ?string $id)
    {
        return $this->request(Model\Ticket::class, 'Get_Ticket_List', [$productId, $id]);
    }

    /** @return Model\Inventory[] */
    public function findInventories(string $merchantId, string $priceId, \DateTimeImmutable $from, \DateTimeImmutable $thru, bool $timeSharing = false)
    {
        return $this->request(Model\Inventory::class, !$timeSharing ? 'GetRealTimeStorage' : 'Time_Share_Price_And_Storage', [
            $merchantId,
            $priceId,
            $from->setTimezone($this->tz)->format('Y-m-d'),
            $thru->setTimezone($this->tz)->format('Y-m-d'),
        ]);
    }

    public function validateIdNumber(string $idNumber)
    {
        /** @var Status $status */
        $status = $this->fetch(Status::class, 'Check_PersonID', [$idNumber]);

        return Status::STATUS_OK === $status->getOk();
    }

    public function dryrun(Model\OrderOptions $options)
    {
        /** @var Status $status */
        $status = $this->fetch(Status::class, 'OrderPreCheck', [
            $options->getTicket(),
            $options->getQuantity(),
            $options->getDate() ? $options->getDate()->setTimezone($this->tz)->format('Y-m-d') : null,
            $options->getPhone(),
            $options->getName(),
            $options->getMerchant(),
            $options->getPaymentMethod(),
            $options->getIdNumber(),
        ]);

        return Status::STATUS_OK === $status->getOk();
    }

    /** @return Model\Order */
    public function create(Model\OrderOptions $options)
    {
        return $this->fetch(Model\Order::class, 'PFT_Order_Submit', [
            $options->getProduct(),
            $options->getTicket(),
            $options->getPermanentId(),
            0,
            $options->getQuantity(),
            $options->getDate() ? $options->getDate()->setTimezone($this->tz)->format('Y-m-d') : null,
            $options->getName(),
            $options->getPhone(),
            $options->getPhone(),
            $options->isSilent() ? 1 : 0,
            $options->getPaymentMethod(),
            0,
            null,
            null,
            0,
            0,
            $options->getMerchant(),
            $options->getIdNumber(),
            null,
            $options->getCallbackURL(),
        ]);
    }

    public function update(string $id, array $updates)
    {
        $params = ['num' => -1, 'ordertel' => null];
        /** @var Model\OrderUpdate $update */
        foreach ($updates as $update) {
            $params = array_merge($params, $update->compose());
        }

        /** @var Model\Refund $refund */
        return $this->fetch(Model\Refund::class, 'Order_Change_Pro', [
            $id,
            $params['num'],
            $params['ordertel'],
        ]);
    }

    public function sendSMS(string $id)
    {
        /** @var Status $status */
        $status = $this->fetch(Status::class, 'reSend_SMS_Global_PL', [$id]);

        return Status::STATUS_OK === $status->getOk();
    }

    public function findCode(string $id)
    {
        /** @var Model\VerificationCode $code */
        $code = $this->fetch(Model\VerificationCode::class, 'Terminal_Code_Verify', [$id]);

        return $code->getCode();
    }

    public function findMoney($type)
    {
        return $this->fetch(Money::class, 'PFT_Member_Fund', [$type], [[
            Money::TYPE_BALANCE => 'balance',
            Money::TYPE_USED_CREDITS => 'used_credits',
            Money::TYPE_TOTAL_CREDITS => 'total_credits',
        ][$type]]);
    }

    public function recv(ServerRequestInterface $request, bool $bypass = false)
    {
        /** @var AbstractNotification $notification */
        $notification = $this->serializer->deserialize($request->getBody(), AbstractNotification::class, 'json');
        if (!$bypass && strtolower($notification->getSignature()) !== strtolower(md5($this->username.$this->password))) {
            return null;
        }

        return $notification;
    }

    public function ackn(bool $ping = false): ResponseInterface
    {
        return $this
            ->responseFactory
            ->createResponse()
            ->withHeader('Content-Type', 'text/plain')
            ->withBody($this->streamFactory->createStream($ping ? 'success' : '200'))
        ;
    }

    public function getContents(): ?string
    {
        return $this->contents;
    }

    private function fetch(string $type, string $method, array $params, array $serializationGroups = [])
    {
        return current($this->request($type, $method, $params, $serializationGroups));
    }

    private function request(string $type, string $method, array $params, array $serializationGroups = [])
    {
        $this->contents = $this->transform($this->soap->__call($method, array_merge([
            $this->username,
            $this->password,
        ], $params)));

        /** @var Result $result */
        $result = $this->serializer->deserialize($this->contents, Template::bind(Result::class, Model\Error::class), 'xml');
        /** @var Model\Error $e */
        $e = current($result->getData());
        $e->raise();

        $context = DeserializationContext::create()->setGroups(array_merge(['Default'], $serializationGroups));
        $result = $this->serializer->deserialize($this->contents, Template::bind(Result::class, $type), 'xml', $context);

        return $result->getData();
    }

    private function transform(string $contents)
    {
        $stylesheet = new \DOMDocument();
        $stylesheet->loadXML(<<<__XSL__
<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="/Rec">
    <Data>
      <xsl:for-each select="items">
        <Rec>
          <xsl:apply-templates select="node()" />
        </Rec>
      </xsl:for-each>
    </Data>
  </xsl:template>
</xsl:stylesheet>
__XSL__
        );

        $document = new \DOMDocument();
        $document->loadXML($contents);

        $p = new \XSLTProcessor();
        $p->importStylesheet($stylesheet);

        return $p->transformToXml($document);
    }
}
