<?php

namespace AzureSpring\Piaofutong\Model;

class Ticket
{
    const ID_NUMBER_VOLUNTARY = 0;
    const ID_NUMBER_REQUIRED = 1;
    const ID_NUMBER_PER_PERSON = 2;

    const VALIDITY_NORMAL = 0;  /* from booking date */
    const VALIDITY_ALWAYS = 1;  /* from the very beginning */
    const VALIDITY_RANGE = 2;   /* time range, valid from / thru */

    /** @var string */
    private $id;

    /** @var string */
    private $productId;

    /** @var string */
    private $merchantId;

    /** @var string */
    private $title;

    /** @var int */
    private $idNumberMandatory;

    /**
     * Min advance reservation.
     *
     * @var int
     */
    private $minReservation;

    /**
     * Max advance reservation.
     *
     * @var int|null
     */
    private $maxReservation;

    /**
     * Reservation closing hour.
     *
     * @var \DateTimeImmutable
     */
    private $closeAt;

    /** @var int */
    private $validity;

    /**
     * Not applicable unless VALIDITY_RANGE.
     *
     * @var bool
     */
    private $notVerifiableInAdvance;

    /** @var \DateTimeImmutable|null */
    private $validFrom;

    /** @var \DateTimeImmutable|null */
    private $validThru;

    /** @var int|null */
    private $priceId;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getProductId(): string
    {
        return $this->productId;
    }

    /**
     * @return string
     */
    public function getMerchantId(): string
    {
        return $this->merchantId;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getIdNumberMandatory(): int
    {
        return $this->idNumberMandatory;
    }

    /**
     * @return int
     */
    public function getMinReservation(): int
    {
        return $this->minReservation;
    }

    /**
     * @return int|null
     */
    public function getMaxReservation(): ?int
    {
        return $this->maxReservation < 0 ? null : $this->maxReservation;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCloseAt(): \DateTimeImmutable
    {
        return $this->closeAt;
    }

    /**
     * @return int
     */
    public function getValidity(): int
    {
        return $this->validity;
    }

    /**
     * @return bool
     */
    public function isNotVerifiableInAdvance(): bool
    {
        return $this->notVerifiableInAdvance;
    }

    /**
     * @return \DateTimeImmutable|null
     */
    public function getValidFrom(): ?\DateTimeImmutable
    {
        return self::VALIDITY_RANGE !== $this->validity ? null : $this->validFrom;
    }

    /**
     * @return \DateTimeImmutable|null
     */
    public function getValidThru(): ?\DateTimeImmutable
    {
        return self::VALIDITY_RANGE !== $this->validity ? null : $this->validThru;
    }

    /**
     * @return int|null
     */
    public function getPriceId(): ?int
    {
        return $this->priceId;
    }
}
