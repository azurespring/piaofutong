<?php

namespace AzureSpring\Piaofutong\Model;

class OrderOptions
{
    const PAYMENT_METHOD_BALANCE = 0;
    const PAYMENT_METHOD_CREDITS = 2;
    const PAYMENT_METHOD_ONSITE = 4;

    /** @var string|null */
    private $permanentId;

    /** @var string|null */
    private $ticket;

    /** @var string|null */
    private $product;

    /** @var string|null */
    private $merchant;

    /** @var int|null */
    private $quantity;

    /** @var string|null */
    private $name;

    /** @var string|null */
    private $phone;

    /** @var string|null */
    private $idNumber;

    /** @var \DateTimeImmutable|null */
    private $date;

    /** @var int */
    private $paymentMethod = self::PAYMENT_METHOD_BALANCE;

    /** @var bool */
    private $silent = false;

    /** @var string|null */
    private $callbackURL;

    public static function create()
    {
        return new OrderOptions();
    }

    /**
     * @return string|null
     */
    public function getPermanentId(): ?string
    {
        return $this->permanentId;
    }

    /**
     * @param string|null $permanentId
     *
     * @return $this
     */
    public function setPermanentId(?string $permanentId): self
    {
        $this->permanentId = $permanentId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTicket(): ?string
    {
        return $this->ticket;
    }

    /**
     * @param string|null $ticket
     *
     * @return $this
     */
    public function setTicket(?string $ticket): self
    {
        $this->ticket = $ticket;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getProduct(): ?string
    {
        return $this->product;
    }

    /**
     * @param string|null $product
     *
     * @return $this
     */
    public function setProduct(?string $product): self
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMerchant(): ?string
    {
        return $this->merchant;
    }

    /**
     * @param string|null $merchant
     *
     * @return $this
     */
    public function setMerchant(?string $merchant): self
    {
        $this->merchant = $merchant;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    /**
     * @param int|null $quantity
     *
     * @return $this
     */
    public function setQuantity(?int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return $this
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     *
     * @return $this
     */
    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getIdNumber(): ?string
    {
        return $this->idNumber;
    }

    /**
     * @param string|null $idNumber
     *
     * @return $this
     */
    public function setIdNumber(?string $idNumber): self
    {
        $this->idNumber = $idNumber;

        return $this;
    }

    /**
     * @return \DateTimeImmutable|null
     */
    public function getDate(): ?\DateTimeImmutable
    {
        return $this->date;
    }

    /**
     * @param \DateTimeImmutable|null $date
     *
     * @return $this
     */
    public function setDate(?\DateTimeImmutable $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return int
     */
    public function getPaymentMethod(): int
    {
        return $this->paymentMethod;
    }

    /**
     * @param int $paymentMethod
     *
     * @return $this
     */
    public function setPaymentMethod(int $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSilent(): bool
    {
        return $this->silent;
    }

    /**
     * @param bool $silent
     *
     * @return $this
     */
    public function setSilent(bool $silent): self
    {
        $this->silent = $silent;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCallbackURL(): ?string
    {
        return $this->callbackURL;
    }

    /**
     * @param string|null $callbackURL
     *
     * @return $this
     */
    public function setCallbackURL(?string $callbackURL): self
    {
        $this->callbackURL = $callbackURL;

        return $this;
    }
}
