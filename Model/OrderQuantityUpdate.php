<?php

namespace AzureSpring\Piaofutong\Model;

class OrderQuantityUpdate implements OrderUpdate
{
    private $quantity;

    public function __construct(int $quantity)
    {
        $this->quantity = $quantity;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function compose(): array
    {
        return ['num' => $this->getQuantity()];
    }
}
