<?php

namespace AzureSpring\Piaofutong\Model;

class Money
{
    const TYPE_BALANCE = 0;
    const TYPE_USED_CREDITS = 1;
    const TYPE_TOTAL_CREDITS = 2;

    /** @var string */
    private $subtotal;

    public function getSubtotal(): string
    {
        return $this->subtotal;
    }
}
