<?php

namespace AzureSpring\Piaofutong\Model;

class Order
{
    /** @var string */
    private $id;

    /** @var string */
    private $permanentId;

    /** @var string|null */
    private $code;

    /** @var string|null */
    private $qrcode;

    /** @var string|null */
    private $url;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPermanentId(): string
    {
        return $this->permanentId;
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @return string|null
     */
    public function getQrcode(): ?string
    {
        return $this->qrcode;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }
}
