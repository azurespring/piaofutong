<?php

namespace AzureSpring\Piaofutong\Model;

class Status
{
    const STATUS_OK = '100';

    /** @var string|null */
    private $ok;

    public function getOk(): ?string
    {
        return $this->ok;
    }
}
