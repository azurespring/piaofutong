<?php

namespace AzureSpring\Piaofutong\Model;

class VerificationCode
{
    /** @var string */
    private $code;

    public function getCode(): string
    {
        return $this->code;
    }
}
