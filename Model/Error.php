<?php

namespace AzureSpring\Piaofutong\Model;

use AzureSpring\Piaofutong\Exception;

class Error
{
    private $code;

    private $message;

    public function raise()
    {
        if (null !== $this->code) {
            throw new Exception($this->message, $this->code);
        }
    }
}
