<?php

namespace AzureSpring\Piaofutong\Model;

class Inventory
{
    /** @var \DateTimeImmutable */
    private $date;

    /** @var string */
    private $costPrice;

    /** @var string */
    private $price;

    /**
     * @return \DateTimeImmutable
     */
    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getCostPrice(): string
    {
        return $this->costPrice;
    }

    /**
     * @return string
     */
    public function getPrice(): string
    {
        return $this->price;
    }
}
