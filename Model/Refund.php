<?php

namespace AzureSpring\Piaofutong\Model;

class Refund extends Status
{
    const STATUS_NOOP = '141';
    const STATUS_PING = '1095';
    const STATUS_ACCEPTED = '1097';

    /** @var string|null */
    private $fee;

    /** @var string|null */
    private $subtotal;

    public function getFee(): ?string
    {
        return $this->fee;
    }

    public function getSubtotal(): ?string
    {
        return $this->subtotal;
    }
}
