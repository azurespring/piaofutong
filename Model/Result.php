<?php

namespace AzureSpring\Piaofutong\Model;

use AzureSpring\Piaofutong\Annotation\Template;

/** @Template({"T"}) */
class Result
{
    /** @var array */
    private $data;

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }
}
