<?php

namespace AzureSpring\Piaofutong\Model;

class OrderPhoneUpdate implements OrderUpdate
{
    private $phone;

    public function __construct(string $phone)
    {
        $this->phone = $phone;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function compose(): array
    {
        return ['ordertel' => $this->getPhone()];
    }
}
