<?php

namespace AzureSpring\Piaofutong\Model;

class Product
{
    /** @var string */
    private $id;

    /** @var string */
    private $title;

    /** @var bool */
    private $timeSharing;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return bool
     */
    public function isTimeSharing(): ?bool
    {
        return $this->timeSharing;
    }
}
