<?php

namespace AzureSpring\Piaofutong\Model;

interface OrderUpdate
{
    public function compose(): array;
}
